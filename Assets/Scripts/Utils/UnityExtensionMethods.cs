﻿using UnityEngine;

public static class UnityExtensionMethods
{
    // Since transforms return their position as a property,
    // you can't set the x/y/z values directly, so you have to
    // store a temporary Vector3
    // Or you can use these methods instead

    /// <summary>
    /// Set the transform X component directly
    /// </summary>
    /// <param name="transform">The transform instance to modify</param>
    /// <param name="x">Desired x component</param>
    public static void SetX(this Transform transform, float x)
    {
        var pos = transform.position;
        pos.x = x;
        transform.position = pos;
    }

    /// <summary>
    /// Set the transform Y component directly
    /// </summary>
    /// <param name="transform">The transform instance to modify</param>
    /// <param name="y">Desired Y component</param>
    public static void SetY(this Transform transform, float y)
    {
        var pos = transform.position;
        pos.y = y;
        transform.position = pos;
    }

    /// <summary>
    /// Set the transform Z component directly
    /// </summary>
    /// <param name="transform">The transform instance to modify</param>
    /// <param name="z">Desired z component</param>
    public static void SetZ(this Transform transform, float z)
    {
        var pos = transform.position;
        pos.z = z;
        transform.position = pos;
    }
}
