﻿using UnityEngine;

public class Board : Singleton<Board>
{
    #region public members

    [Header("Default Tile")]
    public GameObject tilePrefab;

    public float width { get; private set; }
    public float heigth { get; private set; }
    public float top { get; private set; }
    public float bottom { get; private set; }
    public float left { get; private set; }
    public float right { get; private set; }

    public float tileHeight { get; set; }
    public float tileWidth { get; set; }

    #endregion

    #region private members

    private Camera camera;

    #endregion

    #region Unity Events


    #endregion

    #region Public Methods

    public void Initialize()
    {
        camera = Camera.main;

        heigth = camera.orthographicSize * 2.0f;
        width = camera.aspect * heigth;
        bottom = -heigth / 2.0f;
        top = heigth / 2.0f;
        left = -width / 2.0f;
        right = width / 2.0f;

        camera.transform.position = new Vector3(right, top, camera.transform.position.z);

        tileWidth = width / 4;
        tileHeight = heigth / 4;

        SetUpTiles();
    }

    #endregion

    #region Private Methods

    private void SetUpTiles()
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                Tile tile = Instantiate(tilePrefab, new Vector3(j * tileWidth, i * tileHeight), Quaternion.identity).GetComponent<Tile>();
                tile.SetScale(tileWidth, tileHeight);
            }
        }
        EventManager.Instance.TriggerEvent(new StartGameEvent());
    }

    #endregion
}
