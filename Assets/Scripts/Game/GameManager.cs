﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Unity Events

    void Start()
    {
        //Initialize the Board and SetUp the initial Tiles
        Board.Instance.Initialize();
    }

    #endregion


}
