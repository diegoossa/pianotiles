﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    #region public members


    #endregion

    #region private members

    private SpriteRenderer sprite;
    private bool isMoving;

    #endregion

    #region Unity Events

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    void OnMouseDown()
    {
        sprite.color = Color.red;
    }

    void Update()
    {
        if (!isMoving)
            return;

        transform.Translate(-transform.up * Time.deltaTime);
        if (transform.position.y <= -Board.Instance.tileHeight)
        {
            transform.SetY(Board.Instance.heigth);
        }
    }

    #endregion

    #region Public Members

    public void SetScale(float x, float y)
    {
        transform.localScale = new Vector3(x, y);
    }

    #endregion

    #region Events

    void OnStartGame(StartGameEvent evt)
    {
        isMoving = true;
    }

    void OnEnable()
    {
        EventManager.Instance.AddListener<StartGameEvent>(OnStartGame);
    }

    void OnDestroy()
    {
        EventManager.Instance.RemoveListener<StartGameEvent>(OnStartGame);

    }

    #endregion


}
